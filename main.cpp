#include <iostream>
#include <cmath>
#define SCALE 255
void filling(int x[SCALE][SCALE], int size) {
    for (int j = 1; j <= size; j++) {
        for (int i = 1; i <= size; i++) {
            x[j][i] = rand() % (27) - 14;
        }
    }
}

void print_matrix(int x[SCALE][SCALE], int size) {
    using namespace std;
    cout << "Filled matrix:" << endl;
    for (int j = 1; j <= size; j++) {
        for (int i = 1; i <= size; i++) {
            cout << x[j][i] << " ";
        }

        cout << endl;
    }
    cout << endl;
}

int* multipilcation_after_max(int x[SCALE][SCALE], int size, int multipilcation[SCALE]) {

    bool log = false;
    int localmax = -99;
    int maxarr[SCALE];
    int temp_multipilcation = 1;
    for (int j = 1; j <= size; j++) {
        for (int i = 1; i <= size; i++) {
            if (x[j][i] >= localmax) {
                localmax = x[j][i];
            }
        }
        maxarr[j] = localmax;

        localmax = 0;
    }
    for (int j = 1; j <= size; j++) {
        for (int i = 1; i <= size; i++) {
            if (x[j][i] >= maxarr[j]) {
                log = true;
            }
            if (log) {
                temp_multipilcation *= x[j][i];
            }
        }

        multipilcation[j] = temp_multipilcation / maxarr[j];
        temp_multipilcation = 1;
        log = false;
    }return 0;
}

void print_multipilcation_after_max(int* (multipilcation), int size) {
    using namespace std;
    cout << "Multiplication after max element in each line:" << endl;

    for (int j = 1; j <= size; j++) {
        cout << multipilcation[j] << " ";
    }

    cout << endl << endl;
}




void count_negatives(int x[SCALE][SCALE], int size) {
    using namespace std;
    int k = 0;
    for (int i = 1; i <= size; i++) {
        for (int j = 1; j <= size; j++) {
            if ((i == j) && (x[i][j] < 0)) {
                k += 1;
            }

        }
    }
    cout << "Count of negative elements in main diagonal is equal " << k << endl << endl;
}
void matrix_redaction(int x[SCALE][SCALE], int size) {
    for (int i = 1; i <= size; i++) {
        for (int j = i; j <= size; j++) {
            if (x[i][j] > 0) {
                x[i][j] = 0;
            }
        }

    }
    std::cout << "*Matrix was redacted*" << std::endl;
}


//
using namespace std;

int main()
{
    const int size = 9;
    int n[SCALE][SCALE];
    int n1[SCALE];
    filling(n, size);
    print_matrix(n, size);
    multipilcation_after_max(n, size, n1);
    print_multipilcation_after_max(n1, size);
    count_negatives(n, size);
    matrix_redaction(n, size);
    print_matrix(n, size);
}


